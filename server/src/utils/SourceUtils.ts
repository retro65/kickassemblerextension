/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import { ILine, ISyntax, ISourceRange, IFile } from "../assembler/KickAssembler";
import LineUtils from "./LineUtils";

export default class SourceUtils {

	public static getLines(sourceLines:string[], file:IFile):ILine[]|undefined {

        let lines = [];
        let next = 0;
        let lastScope = [];
        let scope = 0;
        let scopeName = "";
        let lastScopeName = [];
        var sourceLine:string;

        if (sourceLines) {
            
            for (var i = 0; i < sourceLines.length; i++) {

                sourceLine = LineUtils.removeComments(sourceLines[i]); 

                let line = <ILine>{};
                line.number = i;
                line.scope = scope;
                line.scopeName = scopeName;
                line.text = sourceLines[i];
                //line.file = file;

                if (sourceLine) {

                    //	search for { - add to scope
                    if (sourceLine.indexOf("{") >= 0) {
                        lastScope.push(scope);
                        lastScopeName.push(scopeName);
                        next += 1;
                        scope = next;

                        //  look for new scope name

                        if (sourceLine.indexOf(".namespace") >= 0) {
                            var tokens = sourceLine.split(" ");
                            var token = tokens[1];
                            scopeName = token.trim();
                        }

                        if (sourceLine.indexOf(":") >= 0) {
                            var token = sourceLine.substring(0, sourceLine.indexOf(":"));
                            scopeName = token.trim();
                        }

                    }
                    
                    //	search for } - remove from scope
                    if (sourceLine.indexOf("}") >= 0) {
                        scope = lastScope.pop();
                        scopeName = lastScopeName.pop();
                    }
                }

                lines.push(line);
            }
        }
        return lines;
	}

	public static getLines2(sourcelines:string[]):ILine[]|undefined {
		var lines = [];
		var next = 0;
		
		/**
		 * something
		 */
		var scope = 0;	//	something
		var last = []
		for(var i = 0; i < sourcelines.length; i++) {

			var source = LineUtils.removeComments(sourcelines[i]); 

			if (source) {

				//	search for { - add to scope
				if (source.indexOf("{") >= 0) {
					next += 1;
					last.push(scope);
					scope = next;
				}
				//	search for } - remove from scope
				if (source.indexOf("}") >= 0) {
					scope = last.pop();
				}
			}

			//	add new line info object
			var newline = <ILine> {};
			newline.number = i+1;
			newline.scope = scope;
			newline.text = sourcelines[i];
			lines.push(newline);
		}
		return lines;
    }
    
    //  create a symbol from source
    public static createSymbol(syntax:ISyntax, range:ISourceRange, lines:ILine[]) {
        
    }
    
        
}