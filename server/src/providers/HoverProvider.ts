/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import {
	Hover,
	IConnection,
	ResponseError,
	TextDocumentPositionParams,
	TextDocument,
} from "vscode-languageserver";

import LanguageDefinition from "../definitions/LanguageDefinition";
import LineUtils from "../utils/LineUtils";
//import { IAssemblerResult, ISymbol, SymbolTypes, ILine } from "./Assembler";
import { IProjectInfoProvider, Provider } from "./Provider";
import { IAssemblerResults, ISyntax, IPreprocessorDirective, IDirective, ILine } from "../assembler/KickAssembler";
import { ISymbol } from "../project/Project";
import { SymbolTypes } from "./Assembler";
import { parse } from "path";

export default class HoverProvider extends Provider {

	constructor(connection:IConnection, projectInfoProvider:IProjectInfoProvider) {
        log.trace("[HoverProvider]");
		super(connection, projectInfoProvider);

		connection.onHover((textDocumentPosition) => {
			log.trace("[HoverProvider] onHover");
			return this.hoverProcess(textDocumentPosition);
		});
	}

	/**
	 * Returns hover information
	 */
	public hoverProcess(textDocumentPositionParams:TextDocumentPositionParams):Hover|ResponseError<void> {

		log.trace("[HoverProvider] hoverProcess");

		//if (this.getProjectInfo().getAssemblerResults().status > 0) {
		//	return null;
		//}

		var NumberConverter = require("number-converter").NumberConverter;
		
		const settings = this.getProjectInfo().getSettings();
		
		//	find line we are on
		const lineNumber = textDocumentPositionParams.position.line;
		const sourceLines = this.getProjectInfo().getSource();
		const assemblerResults = this.getProjectInfo().getAssemblerResults();
		const line:ILine = this.getProjectInfo().getLines(textDocumentPositionParams.textDocument)[lineNumber];

		if (sourceLines && assemblerResults && !isNaN(lineNumber) && sourceLines.length > lineNumber) {

			// 	get the token at the current postion
			const sourceLine = LineUtils.removeComments(sourceLines[lineNumber]);
			if (sourceLine) {
				const character = textDocumentPositionParams.position.character;
				const token = LineUtils.getTokenAtSourcePosition(sourceLines, lineNumber, character);
				log.trace(`token is [${token}]`);
				if (token) {
					// Will search for valid hover strings based on the target
					let contents:string[]|undefined;

					const range = LineUtils.getTokenRange(sourceLine, token, lineNumber, character);

					let fullToken = sourceLine.substr(range.start.character-1, range.end.character-(range.start.character-1));

					/*
						i think this value stuff can/should be done better
						for now it kinda works ok
					*/

					//	TODO: fix this

					//	hex only
					if (fullToken.substr(0,1) == "$") {
						var nc = new NumberConverter(NumberConverter.HEXADECIMAL, NumberConverter.DECIMAL);
						var num = nc.convert(token.toUpperCase());
						contents = [this.getFormattedValue(num)];
					}

					//	binary
					if (fullToken.substr(0,1) == "%") {
						var nc = new NumberConverter(NumberConverter.BINARY, NumberConverter.DECIMAL);
						var num = nc.convert(token.toUpperCase());
						contents = [this.getFormattedValue(num)];
					}

					//	hex
					if (fullToken.substr(0,2) == "#$") {
						var nc = new NumberConverter(NumberConverter.HEXADECIMAL, NumberConverter.DECIMAL);
						var num = nc.convert(token.toUpperCase());
						contents = [this.getFormattedValue(num)];
					}

					//	decimal
					if (fullToken.substr(0,1) == "#") {
						contents = [this.getFormattedValue(parseInt(token))];
					}

					// Check if the target is an instruction
					if (!contents) contents = this.getInstructionHover(token);

                    //  check for pre-processor command
                    if (!contents) contents = this.getPreProcessorHover(token, assemblerResults.assemblerInfo.preprocessorDirectives);

					//	check for directives
					if (!contents) contents = this.getDirectiveHover(token, assemblerResults.assemblerInfo.directives);

					// Check if the target is a symbol or label
					//if (!contents) contents = this.getSymbol(this.getProjectInfo().getResults(), token, line);
					if (!contents) contents = this.getSymbol(token, this.getProjectInfo().getSymbols(), line);
					
					if (contents) {
						return {
							contents,
							range,
						};
					}
				}
			}
		}

		return  { contents: [] };
	}

	private getInstructionHover(target:string):string[]|undefined {
		const instructionMatch = LanguageDefinition.Instructions.find((instruction) => {
			return instruction.name.toLowerCase() === target.toLowerCase();
		});
		if (instructionMatch) {
			return [
				`(instruction) \`${instructionMatch.name}\` : ${instructionMatch.description}`,
			];
		}
	}

    private getDirectiveHover(target:string, items:IDirective[]):string[]|undefined {
		
		const match = items.find((item) => {
			return item.name.substr(1, item.name.length - 1).toLowerCase() === target.toLowerCase();
		});

		if (match) {
			return [
				`(pre-processor) \`${match.name}\` : ${match.description}`,
			];
		}
		
	}
			
	private getPreProcessorHover(target:string, items:IPreprocessorDirective[]):string[]|undefined {

		const match = items.find((item) => {
			return item.name.substr(1, item.name.length - 1).toLowerCase() === target.toLowerCase();
		});

        if (match) {
            return [
                `(pre-processor) \`${match.name}\` : ${match.description}`,
            ];
		}
		
	}
	
	private getSymbol( target:string, symbols:ISymbol[], line:ILine):string[]|undefined {

		log.trace("[HoverProvider] getSymbol");
		log.trace(`target: ${target}, line:${line}`);

		let symbol:ISymbol = this.getHighestScopeSymbol(symbols, target, line);

		if (symbol) {

			if (symbol.type == SymbolTypes.Label) {
				return [
					`(label) \`${symbol.name}\``,
					symbol.detail,
				];
			}

			if (symbol.type == SymbolTypes.Variable) {
				return [
					`(variable) \`${symbol.name}\``,
					symbol.detail,
				];
			}
		}

		return [`(unknown) \`${target}\``];
	}

	/*
    private getSymbol(results:IAssemblerResult, target:string, line:ILine):string[]|undefined {
		if (results) {
			//let symbol:ISymbol = results.symbols.find((symbol) => symbol.name === target);
			let symbol:ISymbol = this.getHighestScopeSymbol(results.symbols, target, line)
			if (symbol) {
				if (symbol.symbolType == SymbolTypes.Constant) {
					return [
						`(constant) \`${symbol.name}\``,
						symbol.detail,
					];
				}
				if (symbol.symbolType == SymbolTypes.Label) {
					return [
						`(label) \`${symbol.name}\``,
						symbol.detail,
					];
				}
				if (symbol.symbolType == SymbolTypes.Variable) {
					return [
						`(variable) \`${symbol.name}\``,
						symbol.detail,
					];
				}
				if (symbol.symbolType == SymbolTypes.Function) {
					return [
						`(function) \`${symbol.name}\``,
						symbol.detail,
					];
				}
				if (symbol.symbolType == SymbolTypes.Macro) {
					return [
						`(macro) \`${symbol.name}\``,
						symbol.detail,
					];
				}
				if (symbol.symbolType == SymbolTypes.PseudoCommand) {
					return [
						`(pseudocommand) \`${symbol.name}\``,
						symbol.detail,
					];
				}
				if (symbol.symbolType == SymbolTypes.Namespace) {
					return [
						`(namespace) \`${symbol.name}\``,
						symbol.detail,
					];
				}
			}
		}
    }
	*/
	
	private getFormattedValue(value:number):string {
		return "* Decimal: `" + value + "`\n\n" +
			"* Binary: `%" + value.toString(2) + "`\n\n" +
			"* Octal: `0" + value.toString(8) + "`\n\n" +
			"* Hexa: `$" + value.toString(16) + "`\n\n";
	}

	private getHighestScopeSymbol(symbols:ISymbol[], target:string, line:ILine):ISymbol {
		var sortBy = require('sort-by'),
			scope_symbols = [];

		//	find matching symbols that are within scope
		for(var i = 0; i < symbols.length; i++) {
			if (symbols[i].name == target) {
				if (symbols[i].scope <= line.scope) {
					scope_symbols.push(symbols[i]);
				}
			}
		}
		
		//	return the highest scoped variable
		scope_symbols.sort(sortBy('-scope'))
		var symbol = scope_symbols[0];
		return symbol;
	}
}

