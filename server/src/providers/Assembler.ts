/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

let tmp = require('tmp');

import { ISettings } from "../providers/SettingsProvider";
import ProjectManager from "../project/ProjectManager";

import PathUtils from "../utils/PathUtils";
import StringUtils from "../utils/StringUtils";
import LineUtils from "../utils/LineUtils";
import LanguageDefinition from "../definitions/LanguageDefinition";

import {
	CompletionItemKind,
	IConnection,
	InitializeResult,
	TextDocument,
	TextDocuments,
} from "vscode-languageserver";

import {
	spawnSync,
} from 'child_process';

import {
	writeFileSync,
	unlinkSync,
	readFileSync,
} from 'fs';

export enum SymbolTypes {
	Label,
	Constant,
	Function,
	Macro,
	PseudoCommand,
	Variable,
	Namespace
}

export interface ISymbol {
	name: string;
	isLabel: boolean;
	symbolType: SymbolTypes;
	itemKind: CompletionItemKind;
	isConstant: boolean;
	value: string;
	detail: string;
	wasReferenced: boolean;
	wasPseudoOpCreated: boolean;
	definitionFilename: string | undefined;
	definitionLineNumber: number;
	lineNumber: number;
	scope: number;
};

export class Symbol implements ISymbol {
	name: string;
	isLabel: boolean;
	symbolType: SymbolTypes;
	itemKind: CompletionItemKind;
	isConstant: boolean;
	value: string;
	detail: string;
	wasReferenced: boolean;
	wasPseudoOpCreated: boolean;
	definitionFilename: string | undefined;
	definitionLineNumber: number;
	lineNumber: number;
	scope: number;
}

export interface ILine {
	number: number;
	filename: string | undefined;
	scope: number;
};

export class Line implements ILine {
	number: number;
	filename: string;
	scope: number;
}


export interface ISourceRange {
	startLine: number,
	startPosition: number,
	endLine: number,
	endPosition: number,
	fileIndex: number,
}

export class SourceRange implements ISourceRange {
	startLine: number;
	startPosition: number;
	endLine: number;
	endPosition: number;
	fileIndex: number;
}

export interface IAssemblerError {
	level: string,
	range: ISourceRange,
	message: string,
}

export class AssemblerError implements IAssemblerError {
	level: string;
	range: ISourceRange;
	message: string;
}

export interface IAssemblerResult {
	errors: IAssemblerError[],
	output: string[],
	symbols: ISymbol[];
	success: boolean,
};

export class AssemblerResult implements IAssemblerResult {
	errors: AssemblerError[];
	output: string[];
	symbols: ISymbol[];
	success: boolean;
}

export class Assembler {

    constructor() {
    }

	public assemble(settings:ISettings, document:TextDocument):IAssemblerResult {

		//	create tmp files for source and assembly infos
		var pathname = PathUtils.getPathFromFilename(PathUtils.uriToPlatformPath(document.uri));
		var tmp_file = tmp.tmpNameSync({ dir: pathname, keep: true });
		var asm_file = tmp.tmpNameSync({ dir: pathname, keep: true });
		writeFileSync(tmp_file, document.getText());

		var tmp_file_lines = StringUtils.splitIntoLines(document.getText());
		var scopeLines = LineUtils.getLines(StringUtils.splitIntoLines(document.getText()));

		//	compile temp file
		let java = spawnSync(settings.javaPath, ["-jar", settings.assemblerPath, tmp_file, '-noeval', '-warningsoff', '-asminfo', 'errors|syntax', '-asminfofile', asm_file], { cwd: pathname });

		//	get errors
		try {  
			var mode = 0;	//	errors
			var data = readFileSync(asm_file, 'utf8');
			var lines = data.toString().split("\n");
			var assemblerErrors = [];
			var symbols = [];

			/*
			for (var i = 0; i < tmp_file_lines.length; i++) {
				line = tmp_file_lines[i];
				let loc = line.indexOf(":");
				if (loc > 0) {
					let symbol = new Symbol();
					len = loc;
					var name = line.substr(0, len);
					symbol.name = name;
					symbol.isLabel = true;
					//symbol.value = i + 1;
					symbols.push(symbol);
				}
			}
			*/

			let has_errors = false;

			if (lines.length > 1) {
				for (var i = 0; i < lines.length -1; i++) {
					if (lines[i] > "") {
						var process = true;
						if (lines[i].toLowerCase().replace(/(\r\n|\n|\r)/gm,"") == "[errors]") {
							mode = 0;
							process = false;
						}
						if (lines[i].toLowerCase().replace(/(\r\n|\n|\r)/gm,"") == "[syntax]") {
							mode = 1;
							process = false;
						}
						if (process) {
							if (mode == 0) {
								var sections = lines[i].split(";");
								var range = sections[1].split(",");
								let assemblerError = new AssemblerError();
								assemblerError.level = sections[0];
								assemblerError.message = sections[2];
								let sourceRange = new SourceRange();
								sourceRange.startLine = Number(range[0]) - 1;
								sourceRange.startPosition = Number(range[1]) - 1;
								sourceRange.endLine = Number(range[2]) - 1;
								sourceRange.endPosition = Number(range[3]);
								sourceRange.fileIndex = Number(range[4]);
								assemblerError.range = sourceRange;
								assemblerErrors.push(assemblerError);
								has_errors = true;
							}
							if (mode == 1) {
								var sections = lines[i].split(";");
								var range = sections[1].split(",");
								let sourceRange = new SourceRange();
								sourceRange.startLine = Number(range[0]) - 1;
								sourceRange.startPosition = Number(range[1]) - 1;
								sourceRange.endLine = Number(range[2]) - 1;
								sourceRange.endPosition = Number(range[3]);
								sourceRange.fileIndex = Number(range[4]);
								let type = sections[0];

								// this is a total hack because ka reorders the file types when errors occur
								let line_num = 1;
								if (has_errors) {
									line_num = 0;
								}
								if (sourceRange.fileIndex == line_num) {
									if (type.toLowerCase() == "label") {
										let symbol = new Symbol();
										var len = sourceRange.endPosition - sourceRange.startPosition;
										var line = tmp_file_lines[sourceRange.startLine];
										var name = line.substr(sourceRange.startPosition, len - 1);
										symbol.name = name;
										symbol.isLabel = true;
										symbol.symbolType = SymbolTypes.Label;
										symbol.itemKind = CompletionItemKind.Reference;
										//symbol.value = sourceRange.startLine;
										symbol.lineNumber = sourceRange.startLine + 1;
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
										symbols.push(symbol);
									}
									if (type.toLowerCase() == "directive") {
										var len = sourceRange.endPosition - sourceRange.startPosition;
										var line = tmp_file_lines[sourceRange.startLine];
										var directive = line.substr(sourceRange.startPosition, len);
										if (directive.toLowerCase() == ".label") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											let parms = parmline.split("=");
											let name = parms[0].trim();
											let value = parms[1].trim();
											symbol.name = name;
											symbol.symbolType = SymbolTypes.Label;
											symbol.detail = value;
											symbol.lineNumber = sourceRange.startLine + 1;
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
											symbols.push(symbol);
										}
										if (directive.toLowerCase() == ".const") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											let parms = parmline.split("=");
											let name = parms[0].trim();
											let value = parms[1].trim();
											symbol.name = name;
											symbol.symbolType = SymbolTypes.Constant;
											symbol.detail = value;
											symbol.lineNumber = sourceRange.startLine + 1;
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
											symbols.push(symbol);
										}
										if (directive.toLowerCase() == ".function") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											var func = StringUtils.splitFunction(parmline);
											var name = func[0];
											if (name.substr(0,1) == "@") {
												//	root function
												name = name.substr(1, name.length-1);
											}
											symbol.name = name;
											symbol.symbolType = SymbolTypes.Function;
											//	get prev line
											
											var pline = tmp_file_lines[sourceRange.startLine-1].trim();
											var help = "";
											if (pline.indexOf("/") >= 0) {
												help = pline.replace("//", "").trim();
											}
											
											symbol.lineNumber = sourceRange.startLine + 1;
											var comments = this.getComments(tmp_file_lines, sourceRange.startLine);
											//symbol.detail = StringUtils.joinStringArray(comments);
											symbol.detail = StringUtils.joinStringArray(comments);
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
											symbols.push(symbol);
										}
										if (directive.toLowerCase() == ".var") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											let parms = parmline.split("=");
											let name = parms[0].trim();
											let value = parms[1].trim();
											symbol.name = name;
											symbol.symbolType = SymbolTypes.Variable;
											var comments = this.getComments(tmp_file_lines, sourceRange.startLine);
											//symbol.detail = StringUtils.joinStringArray(comments);
											symbol.detail = StringUtils.joinStringArray(comments);
											symbol.lineNumber = sourceRange.startLine + 1;
											symbol.scope = scopeLines[sourceRange.startLine].scope;
											symbols.push(symbol);
										}
										if (directive.toLowerCase() == ".namespace") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											let parms = parmline.split(" ");
											let name = parms[0].trim();
											let value = parms[1].trim();
											symbol.name = name;
											symbol.symbolType = SymbolTypes.Namespace;
											symbol.detail = "";
											symbol.lineNumber = sourceRange.startLine + 1;
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
											symbols.push(symbol);
										}
										if (directive.toLowerCase() == ".macro") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											var func = StringUtils.splitFunction(parmline);
											var name = func[0];
											if (name.substr(0,1) == "@") {
												//	root function
												name = name.substr(1, name.length-1);
											}
											symbol.name = name;
											symbol.symbolType = SymbolTypes.Macro;
											//	get prev line
											
											var pline = tmp_file_lines[sourceRange.startLine-1].trim();
											var help = "";
											if (pline.indexOf("/") >= 0) {
												help = pline.replace("//", "").trim();
											}
											
											symbol.lineNumber = sourceRange.startLine + 1;
											var comments = this.getComments(tmp_file_lines, sourceRange.startLine);
											//symbol.detail = StringUtils.joinStringArray(comments);
											symbol.detail = StringUtils.joinStringArray(comments);
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
											symbols.push(symbol);
										}
										if (directive.toLowerCase() == ".pseudocommand") {
											let symbol = new Symbol();
											let parmline = line.substr(sourceRange.endPosition + 1);
											var func = StringUtils.splitPseudoCommand(parmline);
											var name = func[0];
											if (name.substr(0,1) == "@") {
												//	root function
												name = name.substr(1, name.length-1);
											}
											symbol.name = name;
											symbol.symbolType = SymbolTypes.PseudoCommand;
											//	get prev line
											var pline = tmp_file_lines[sourceRange.startLine-1].trim();
											var help = "";
											if (pline.indexOf("/") >= 0) {
												help = pline.replace("//", "").trim();
											}
											symbol.detail = help;
											symbol.lineNumber = sourceRange.startLine + 1;
											symbol.scope = scopeLines[sourceRange.startLine + 1].scope;
											symbols.push(symbol);
										}
									}
								}
							}
						}
					}
				}
			}
		} catch(e) {
			//console.log('Error:', e.stack); 
		}

		//	populate assembly results
		let assemblerResult = new AssemblerResult();
		assemblerResult.errors = assemblerErrors;
		assemblerResult.output = java.output;
		assemblerResult.symbols = symbols;
		assemblerResult.success = java.status == 0 ? true: false;

		//	remove temp file
		unlinkSync(tmp_file);		
		unlinkSync(asm_file);

		//console.log(assemblerResult);
		return assemblerResult;
	}

	/**
	 * Get the Comments above the line specified
	 * @param lines - array of lines
	 * @param start - starting line
	 */
	private getComments(lines:string[], start:number):string[]|undefined {

		var ret = [];
		//	find comment start
		var cs = LineUtils.findCommentStart(lines, start);

		//	check current line for additional comments
		if (cs >=0) {
			for (var i = cs; i < start; i++) {
				var line = lines[i];
				line = line.replace("/", "");
				line = line.replace("\\", "");
				line = line.replace("*", "");
				line = line.replace("*", "");
				if (line.trim() != "") { 
					ret.push(line.trim());
				}
			}
		}

		//	return a string array

		return ret;
	}
}

export default Assembler;